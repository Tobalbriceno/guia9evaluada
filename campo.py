from fighter import Fighter

class Wizzard(Fighter):
    def __init__(self):
        self._salud = 30
        self._fulldamage = 12
        self._normaldamage = 3
        self._hechizo = False
        self._vulnerabilidad = True
        
    @property
    def salud(self):
        return self._salud
    
    @salud.setter
    def salud(self,estado_salud):
        self._salud = estado_salud
        
    def sigue_vivo(self):
        if self.salud <= 0:
            return False
        return True
    
    @property
    def hechizo(self):
        return self._hechizo

    @hechizo.setter
    def hechizo(self, estado_hechizo):
        self._hechizo = estado_hechizo

    @property
    def vulnerabilidad(self):
        return self._vulnerabilidad

    @vulnerabilidad.setter
    def vulnerabilidad(self, isVulnerable):
        self._vulnerabilidad = isVulnerable
    
    @property
    def damage(self):
        return self._damage
    
    damage.setter
    def damage(self, damagein):
        self._damage = damagein
        
    @property
    def atack(self):
        return self._atack()
    
    @atack.setter
    def atack(self,atack):
        if isinstance(atack):
            self._atack = atack
    
    
    
class Warrior():
    def __init__(self):
        self._salud = 40
        self._fulldamage = 10
        self._normaldamage = 6
    
    def AtackWarrior(self):
        pass
    
    @property
    def saludwarrior(self):
        return self._salud
    
    @saludwarrior.setter
    def saludwarrior(self,estado_salud):
        self._saludwarrior = estado_salud
        
    def sigue_vivo(self):
            if self.saludwarrior <= 0:
                return False
            return True
    
    @property
    def damage(self):
        return self._damage
    
    damage.setter
    def damage(self, damagein):
        self._damage = damagein
        
    @property
    def atack(self):
        return self._atack
    
    @atack.setter
    def atack(self,atack):
        if isinstance(atack):
            self._atack = atack
    
            